const images = document.querySelectorAll('.card');
const congratsMessage = document.getElementById('congratsMessage');

let flipped = false;
let clickCount = 0;
let timerInterval;
let seconds = 0;
let start = document.querySelector('.btn');
let timer;
let gameOn = false; 


const handleClick = () => {
  images.forEach((image) => {
    image.classList.toggle('flipped');
  });
  setTimeout(() => {
    images.forEach((image) => {
      image.classList.toggle('flipped');
    });
  }, 500);

  if (gameOn) {
    stopTimer();
    gameOn = false;
  } else {
    startTimer();
    toGameOn();
  }
  start.removeEventListener('click', handleClick);
};

start.addEventListener('click', handleClick);



const image = document.querySelector('.back');

let flipsCount = 0;


function toGameOn(congratsMessage) {
  let checkImage = [];
  let imageCounter = 0;
  let flipsCount = 0;

  images.forEach((image) => {
    randomizeImages();
    image.addEventListener('click', function () {
      let imageSource = image.querySelector('img.back').getAttribute('src');

      if (!image.classList.contains('flipped') && checkImage.length < 2) {
        checkImage.push(image);
        image.classList.add('flipped');
        flipsCount++;
        updateClickCount(flipsCount); 

        if (checkImage.length === 2) {
          if (
            checkImage[0].querySelector('img.back').getAttribute('src') ===
            checkImage[1].querySelector('img.back').getAttribute('src')
          ) {
            checkImage.forEach((flippedImage) => {
              flippedImage.classList.add('matched');
            });
            checkImage = [];
            imageCounter = imageCounter + 2;
            console.log(imageCounter);

            if (imageCounter === 16) {
              console.log("Congratulations");
              stopTimer();
              displayCongratsMessage();
            }
          } else {
            setTimeout(() => {
              checkImage.forEach((flippedImage) => {
                flippedImage.classList.remove('flipped');
              });
              checkImage = [];
            }, 1000);
          }
        }
      }
    });
  });



  
  function displayCongratsMessage(message) {
    const messageElement = document.getElementById('congrats-message');
    messageElement.textContent = message;
    messageElement.style.display = 'block';
  }
}


function updateClickCount(flipsCount) {
  const clickCountElement = document.querySelector('.move');
  clickCountElement.textContent = `${flipsCount} Moves`;
}



function displayCongratsMessage() {
  var congratsContainer = document.getElementById("congrats-container");
  congratsContainer.style.display = "flex";
}




function stopTimer() {
  clearInterval(timer);
}




function startTimer() {
  timer = setInterval(() => {
    seconds++;
    updateTimer();
  }, 1000);
}

function updateTimer() {
  const timerElement = document.querySelector('.timer');
  timerElement.textContent = `Timer: ${seconds}s`;
}


function randomizeImages() {
  const cards = document.querySelectorAll('.card');
  cards.forEach(card => {
    const randomPos = Math.floor(Math.random() * cards.length);
    card.style.order = randomPos;
  });
}

